from django.http import HttpResponse
from django.http.response import JsonResponse
from django.shortcuts import render

import numpy as np
from tensorflow.keras.preprocessing import image
from tensorflow.keras import models
from PIL import Image
import io
cnn=models.load_model('model')
def predictor(img_bytes):
    img = Image.frombytes("L",(64,64),img_bytes)
    img = img.convert('RGB')
    img = image.img_to_array(img)
    test_image = image.img_to_array(img)
    test_image = np.expand_dims(test_image, axis = 0)
    result = cnn.predict(test_image)
    if result[0][0] == 1:
        prediction = 'dog'
    else:
        prediction = 'cat'
    return prediction

def index(request):
    return render(request, 'index.html')

def predict(request):
    ans = predictor(eval(request.body.decode("utf-8"))["image"].encode())
    return JsonResponse({"result":ans})