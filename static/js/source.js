const fileInput =  document.getElementById('file')
const imageHolder = document.getElementById('image')
const resultText = document.getElementById('result')
var file;
var result;
const upload = () => {
    fileInput.click();
}
const changeHandler = ()=>{
    file = fileInput.files[0]
    if (file){
        reader = new FileReader();
        reader.onload = ()=>{
            result = reader.result
            imageHolder.src = result
        }
        reader.readAsDataURL(file)
    }
}
const predict = () => {
    fetch("/predict/",{
        method: 'POST', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': token
        },
        body: JSON.stringify({
            image: result,
        }),
    }).then(response => response.json()).then(
        response=>{
            resultText.innerHTML = `This is a ${response.result}`
        }
    )
}